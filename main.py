import sys
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from cal_inspection_gui import Ui_MainWindow as gui

class Cal():
 
    def failCount(self, df):
        """Takes dataframe, returns number of occurences of each fail reason"""
        index_list = []
        for i in range(df.shape[1]):
            index_list.append(chr(i+65))

        fail_list = []
        for j in index_list:
            fail_list.append(self.dataFilter(df,"Fail Reasons", j).shape[0])

        d = pd.DataFrame(data={'# Fail Occurences': fail_list, 'Description': list(df.columns)}, index=index_list)
        return d

    def addFilter(self, filt, disp, column, value, intralogic="==", interlogic="and"): 
        """format filter string for dataframe query and gui filter display"""
        d = "({} == {}".format(column, value[0])
        if type(value[0])==str:
            value[0] = '\"' + value[0] + '\"'
            if column=="Fail Reasons":
                value[0] = '(' + value[0] + ', na=False, case=False)'
                intralogic = ".str.contains"             
        f = '(`{}`{}{})'.format(column, intralogic, value[0])

        for v in value[1:]:
            d += " or {}".format(v)
            if type(v)==str:
                v = '\"' + v + '\"'
                if column=="Fail Reasons":
                    v = '(' + v + ', na=False, case=False)'
                    intralogic = ".str.contains"
            f = '(' + f + ' or (`{}`{}{}))'.format(column, intralogic, v)
        d += ")"

        if filt=="":
            filt = f
            disp = d
        else:
            filt = '(' + filt + ' ' + interlogic + ' ' + f + ')'
            disp = '(' + disp + '\n' + interlogic + ' ' + d + ')'

        return filt, disp

    def dataFilter(self, df, column, value):
        """returns filtered dataframe"""
        try:
            return df[df[column].str.contains(value, na=False)]
        except:
            return df[df[column]==value]

    def histFail(self, df):
        """create bar plot of # of occurences of each fail reason"""
        df.plot.bar(fontsize=14)
        plt.rc('font', size=14)
        plt.title("Occurence of Fail Reasons", wrap=True)
        plt.xlabel("Fail Reasons")
        plt.ylabel("Number of Occurences")
        plt.grid()

    def scatPlot(self, df, xs, ys):
        """create scatter plot of any two numerical columns"""
        df.plot.scatter(xs, ys, fontsize=14)
        plt.rc('font', size=14)
        plt.title("Scatter plot of " + ys + " vs " + xs, wrap=True)
        plt.xlabel(xs)
        plt.ylabel(ys)
        plt.grid()

    def histNum(self, df, column, nbins, log=False):
        """create histogram of any numerical column"""
        data = df[column].dropna()
        plt.figure()
        plt.rc('font', size=14)
        if log==True:
            
            if data.min()==0:
                lbound = 0
            else:
                lexp = np.floor(np.log10(np.absolute(data.min())))
                lbound = np.floor(data.min()/(10**lexp))*(10**lexp)
            
            if data.max()==0:
                ubound = 0
            else:
                uexp = np.floor(np.log10(np.absolute(data.max())))
                ubound = (np.floor(data.max()/(10**uexp))+1)*(10**uexp)

            tr = matplotlib.scale.SymmetricalLogTransform(base=10, linthresh=1, linscale=1)
            ss = tr.transform([lbound, ubound])
            nb = tr.inverted().transform(np.linspace(*ss, num=nbins))
            plt.gca().set_xscale("symlog", subsx=[2, 3, 4, 5, 6, 7, 8, 9])
            plt.gca().xaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(self.ticks_format_major))
            plt.gca().xaxis.set_minor_formatter(matplotlib.ticker.FuncFormatter(self.ticks_format_minor))
        else:
            nb = nbins
        plt.hist(data, bins=nb)
        plt.title("Histogram of " + column, wrap=True)
        plt.xlabel(column)
        plt.ylabel("Number of Occurences")
        plt.grid()

    def ticks_format_major(self, value, index):
        """format major plot ticks (for use in histNum only)"""
        if value==0:
            return '${0:d}$'.format(int(value))
        exp = np.floor(np.log10(np.absolute(value)))
        base = value/10**exp
        if exp==0 or exp==1 or exp==2:   
            return '${0:d}$'.format(int(value))
        if exp==-1:
            return '${0:.1f}$'.format(value)
        else:
            return '${0:d}\\times10^{{{1:d}}}$'.format(int(base), int(exp))        

    def ticks_format_minor(self, value, index):
        """format minor plot ticks (for use in histNum only)"""
        if value==0:
            return None
        exp = np.floor(np.log10(np.absolute(value)))
        base = value/10**exp
        if base==2 or base==5:   
            if exp==0 or exp==1 or exp==2:   
                return '${0:d}$'.format(int(value))
            if exp==-1:
                return '${0:.1f}$'.format(value)
            else:
                return '${0:d}\\times10^{{{1:d}}}$'.format(int(base), int(exp))
        else:
            return None

class MainW (QMainWindow, gui):

    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.cal = Cal()
        self.cal.filt = ""
        self.failDict = {}
        self.filtColList = []
        self.disp = ""
        pd.set_option("display.max_rows", None, "display.max_columns", None)

    def inFileButtonClicked(self):
        """initialize dataframe and populate combo boxes with columns when file is selected"""
        inFileName, _ = QFileDialog.getOpenFileName(self, 'Open File', '.', "Excel files (*.xlsx)")
        if inFileName!="":
            self.cal = Cal()
            self.cal.filt = ""
            self.failDict = {}
            self.filtColList = []

            self.dataf = pd.read_excel(inFileName, sheet_name="MasterSheet", na_values='-')
            self.inFileLabel.setText(inFileName)
            self.dataf.rename(columns=lambda s: s.replace("#", "Number") if s.startswith("#") else s.replace("#", "_Number"), inplace=True)
            colTypes = self.dataf.dtypes
            self.histColList = []
            self.objColList = []
            i = 0
            for c in colTypes:
                self.failDict[chr(i+65)] = chr(i+65) + ': ' + colTypes.index[i]
                if c=='int64' or c=='float64':
                    self.histColList.append(colTypes.index[i])
                else:
                    self.objColList.append(colTypes.index[i])
                i+=1
            self.histColumn.clear()
            self.histColumn.addItems(self.histColList)
            self.xScatColumn.clear()
            self.xScatColumn.addItems(self.histColList)
            self.yScatColumn.clear()
            self.yScatColumn.addItems(self.histColList)
            self.filterColumn.clear()
            self.filterColumn.addItems(self.objColList) 
            self.filtData = self.dataf


    def filtDataFileButtonClicked(self):
        """save filtered dataframe to a new file"""
        outFileName, _ = QFileDialog.getSaveFileName(self, 'Save File', '.', "CSV files (*.csv)")
        try:
            self.filtData.to_csv(outFileName, index=False, na_rep = '-')
        except:
            qErr = QMessageBox()
            qErr.setText('Invalid Input File Selected')
            qErr.exec_()

    def filterColumnChanged(self):
        """display filter options for the selected column"""
        self.valueList.clear()
        column = str(self.filterColumn.currentText())
        if column=="Fail Reasons":
            vFail = self.dataf[column].dropna().unique().astype(str)
            vL = list(set("".join(vFail).replace(" ","")))
            vList = np.vectorize(self.failDict.get)(vL)
            self.valueList.addItems(vList)
        elif column!="":
            try:
                vList = self.dataf[column].dt.strftime("%d %b %Y").unique()
                self.valueList.addItems(vList)
            except:
                vList = self.dataf[column].dropna().unique().astype(str)
                self.valueList.addItems(vList)

    def clearFilter(self):
        """reset filter when clear filter button is clicked"""
        try:
            self.cal.filt = ""
            self.filterDisplay.clear()
            self.filtData = self.dataf
            self.filtColList = []

            obj = self.objColList
            for o in obj:
                if o in {"Fail Reasons", "Comments", "Inspector", "Device Name", "Calibration Verdict"}:
                    obj.remove(o)
            for o in obj:
                if o in {"Fail Reasons", "Comments", "Inspector", "Device Name", "Calibration Verdict"}:
                    obj.remove(o)
            fi = self.filtData[obj]
            self.plotInfo.clear()
            self.plotInfo.setText(str(fi))

        except AttributeError:
            qErr = QMessageBox()
            qErr.setText('Invalid File Selected')
            qErr.exec_()
        except IndexError:
            qErr = QMessageBox()
            qErr.setText('No Value Selected')
            qErr.exec_()

    def appendFilter(self):
        """add filter to query, and update filtered dataframe and info"""
        if self.orButton.isChecked():
            logic = "or"
        else:
            logic = "and"
        try:
            value = []
            for v in self.valueList.selectedItems():
                if self.filterColumn.currentText()=="Fail Reasons":
                    value.append(v.text()[0])
                else:
                    value.append(v.text())

            filt, disp = self.cal.addFilter(self.cal.filt, self.disp, self.filterColumn.currentText(), value, interlogic=logic)
            self.filtData = self.dataf.query(filt, engine="python")

            self.valueList.clearSelection()
            self.filterDisplay.clear()
            self.filterDisplay.append(disp)
            self.cal.filt = filt
            self.disp = disp

            obj = self.objColList
            self.filtColList.append(self.filterColumn.currentText())

            for o in obj:
                if (o in {"Fail Reasons", "Comments", "Inspector", "Device Name", "Calibration Verdict"}) or (o in self.filtColList):
                    obj.remove(o)
            for o in obj:
                if (o in {"Fail Reasons", "Comments", "Inspector", "Device Name", "Calibration Verdict"}) or (o in self.filtColList):
                    obj.remove(o)
            fi = self.filtData[obj]
            self.plotInfo.clear()
            self.plotInfo.setText(str(fi))

        except AttributeError:
            qErr = QMessageBox()
            qErr.setText('Invalid File Selected')
            qErr.exec_()
        except ValueError:
            qErr = QMessageBox()
            qErr.setText('Filter Value Not Found')
            qErr.exec_()
        except IndexError:
            qErr = QMessageBox()
            qErr.setText('No Value Selected')
            qErr.exec_()

    def updatePlot(self):
        """create plot based on selected options and filtered dataframe"""
        try:
            fData = self.filtData
            if self.histSelect.isChecked():
                ns = fData.describe()[self.histColumn.currentText()]
                self.plotInfo.clear()
                self.plotInfo.setText(str(ns))
                self.cal.histNum(fData, self.histColumn.currentText(), int(self.nBinsEdit.text()), self.logSelect.isChecked())
            elif self.scatSelect.isChecked():
                xs = self.xScatColumn.currentText()
                ys = self.yScatColumn.currentText()
                stats = fData.describe()[[xs,ys]]
                self.plotInfo.clear()
                self.plotInfo.setText(str(stats))
                self.cal.scatPlot(fData, xs, ys)
            else:
                fc = self.cal.failCount(fData)
                self.plotInfo.clear()
                self.plotInfo.setText(str(fc))
                self.cal.histFail(fc)
            plt.show()
        except:
            qErr = QMessageBox()
            qErr.setText('Invalid File Selected')
            qErr.exec_()

if __name__=="__main__":
    app = QApplication([])
    window = MainW()
    window.show()
    sys.exit(app.exec_())

