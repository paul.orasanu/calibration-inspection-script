Script & GUI used for calibration inspection
Python script requires pandas 1.1.3, numpy 1.19.2, matplotlib 3.0.3, PyQt5 5.15.1, and xlrd 1.2.0

Executable can be run without needing library installations.